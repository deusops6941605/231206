# Используйте официальный образ Python как родительский образ
FROM python:3.9-slim

# Установите рабочую директорию в контейнере
WORKDIR /usr/src/app

# Скопируйте файлы зависимостей
COPY requirements.txt ./

# Установите зависимости
RUN pip install --no-cache-dir -r requirements.txt

# Скопируйте код проекта в контейнер
COPY . .

# Соберите статические файлы
RUN python manage.py collectstatic --noinput

# Запустите приложение на порту 8000
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "myproject.wsgi:application"]
